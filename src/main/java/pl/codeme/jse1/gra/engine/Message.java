package pl.codeme.jse1.gra.engine;

import java.util.Map;

import pl.codeme.jse1.gra.engine.GameBoard.Coordinate;

/**
 * Interfejs komunikatów wymienianych między Game i CLInterface
 * 
 * @author pawel.apanasewicz@codeme.pl
 *
 */
public interface Message {

    /**
     * Ustawienie przekazywanego tekstu
     * 
     * @param text Przekazywany tekst
     */
    public void setText(String text);

    /**
     * Pobranie przekazywanego tekstu
     * 
     * @return Przekazywany tekst
     */
    public String getText();

    /**
     * Ustawienie do przekazania planszy gry
     * 
     * @param gameBoard Plansza gry
     */
    public void setGameBoardMap(Map<Coordinate, Sign> gameBoardMap);

    /**
     * Metoda służy do pobrania przekazywanej planszy gry
     * 
     * @return Plansza gry
     */
    public Map<Coordinate, Sign> getGameBoardMap();

    /**
     * Metoda sprawdza jaki komenda została podana i zwraca jej reprezentację w postaci interfejsu Message
     * 
     * @param text Komenda gry
     * 
     * @return Komunikat gry
     */
    public Message getMessageByText(String text);

    /**
     * Ustawienie przekazywanego obiektu
     * 
     * @param data Przekazywany obiekt
     */
    public void setData(Object data);

    /**
     * Pobranie przekazywanego obiektu
     * 
     * @return Przekazany obiekt
     */
    public Object getData();

}
