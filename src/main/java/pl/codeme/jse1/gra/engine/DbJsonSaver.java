package pl.codeme.jse1.gra.engine;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Persistence;
import javax.persistence.Table;

import pl.codeme.jse1.gra.engine.GameBoard.Coordinate;

@Entity
@Table(name = "save_game")
public class DbJsonSaver implements Saver {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String  name;

    private String  json;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void save(String name, Object data) throws IOException {
        Map<String, Object> gameData = (Map<String, Object>)data;
        JsonObjectBuilder builder = Json.createObjectBuilder(); // utworzenie głównego obiektu JSON
        Iterator<String> keys = gameData.keySet().iterator(); // pobranie kluczy z mapy danych gry
        while(keys.hasNext()) {
            String fieldName = keys.next(); // pobranie klucza
            // jeżeli nazwa klucza jest gb to obsługujemy mapę planszy gry
            if(!fieldName.equals("gb")) {
                builder.add(fieldName, (String)gameData.get(fieldName)); // dodanie elementów do obiektu głownego JSON
            } else {
                JsonArrayBuilder array = Json.createArrayBuilder(); // przygotowanie tablicy na dane z planszy gry
                Map<Coordinate, Sign> gb = (Map<Coordinate, Sign>)gameData.get(fieldName); // pobranie planszy gry
                Iterator<Coordinate> gbKeys = gb.keySet().iterator();
                while(gbKeys.hasNext()) {
                    Coordinate coord = gbKeys.next();
                    JsonObjectBuilder gbBuilder = Json.createObjectBuilder(); // przygotowanie obiektu z koordynatami i znakiem
                    gbBuilder.add("x", coord.getX());
                    gbBuilder.add("y", coord.getY());
                    gbBuilder.add("v", gb.get(coord).toString());
                    array.add(gbBuilder); // dodanie do tablicy danych planszy
                }
                builder.add("gb", array); // dodanie tablicy danych z planszy do głównego obiektu JSON
            }
        }

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("UsersDS");
        EntityManager em = factory.createEntityManager();

        this.name = name;
        this.json = builder.build().toString();
        em.getTransaction().begin();
        em.persist(this);
        em.getTransaction().commit();

        em.close();
        factory.close();
    }

    @Override
    public Object load(String name) throws IOException {
        // TODO Auto-generated method stub
        return null;
    }

}
