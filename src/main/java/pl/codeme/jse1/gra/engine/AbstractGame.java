package pl.codeme.jse1.gra.engine;

import java.io.IOException;

/**
 * Podstawa gier umozliwiająca zapis i wgrywanie gier
 * 
 * @author pawel.apanasewicz@codeme.pl
 *
 */
public abstract class AbstractGame {

    /**
     * Obiekt obsługujący zapis i wgrywanie gier
     */
    private Saver saver;

    /**
     * Ustawienie obiektu obsługujący zapis i wgrywanie gier
     * @param saver
     */
    protected void setSaver(Saver saver) {
        this.saver = saver;
    }

    /**
     * Metoda wgrywająca gre
     * 
     * @param name Nazwa gry
     * 
     * @return Zapisana gra
     * 
     * @throws IOException
     */
    protected void save(String name, Object data) throws IOException {
        saver.save(name, data);
    }

    /**
     * Metoda zapisuje grę
     * 
     * @param name Nazwa gry
     * @param data Dane gry
     * 
     * @throws IOException
     */
    protected Object load(String name) throws IOException {
        return saver.load(name);
    }

}
