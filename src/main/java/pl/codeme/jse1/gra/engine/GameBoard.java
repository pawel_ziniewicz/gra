package pl.codeme.jse1.gra.engine;

import static pl.codeme.jse1.gra.engine.Sign.EMPTY;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Klasa reprezentująca planszę gry
 * 
 * @author pawel.apanasewicz@codeme.pl
 *
 */
public class GameBoard {

    public enum GameState { WIN, PAT, RUN }

    /**
     * Mapa przechowująca stan gry (ustawione znaki na polach)
     */
    private Map<Coordinate, Sign> gameBoard;

    /**
     * Wysokość planszy (x)
     */
    private int                   height;

    /**
     * Szerokość planszy (y)
     */
    private int                   width;

    /**
     * Konstruktor klasy
     * 
     * @param height Wysokość planszy
     * @param width Szerokość planszy
     */
    public GameBoard(int height, int width) {
        this.height = height;
        this.width = width;
        clear();
    }

    public void loadGameBoardMap(Map<Coordinate, Sign> gameBoardMap) {
        gameBoard = gameBoardMap;
    }

    /**
     * Metoda czyszcząca planszę
     */
    public void clear() {
        if(gameBoard == null) {
            gameBoard = new HashMap<>();
            for(int y = 0; y < height; y++) {
                for(int x = 0; x < width; x++) {
                    gameBoard.put(new Coordinate(y, x), EMPTY);
                }
            }
        } else {
            Iterator<Coordinate> keys = gameBoard.keySet().iterator();
            while(keys.hasNext()) {
                gameBoard.put(keys.next(), EMPTY);
            }
        }
    }

    /**
     * Metoda zwraca Mapę stanu gry
     * 
     * @return Mapa stanu gry
     */
    public Map<Coordinate, Sign> getGameBoard() {
        return gameBoard;
    }

    /**
     * Metoda zwraca szerokość planszy
     * 
     * @return Szerokość planszy
     */
    public int getWidth() {
        return width;
    }

    /**
     * Metoda zwraca wysokość planszy
     * 
     * @return Wysokosc planszy
     */
    public int getHeight() {
        return height;
    }

    /**
     * Metoda wyszukuje klucz (koordynaty)
     * 
     * @param coord Koordynaty poszukiwanego klucza
     * 
     * @return Klucz z mapy planszy
     */
    private Coordinate findKey(Coordinate coord) {
        Iterator<Coordinate> keys = gameBoard.keySet().iterator();
        while(keys.hasNext()) {
            Coordinate key = keys.next();
            if(key.equals(coord)) {
                return key;
            }
        }

        return null;
    }

    /**
     * Czy są jeszcze jakieś puste pola na planszy
     * 
     * @return Czy są puste pola
     */
    private boolean hasEmpty() {
        Iterator<Coordinate> keys = gameBoard.keySet().iterator();
        while(keys.hasNext()) {
            if(gameBoard.get(keys.next()).equals(EMPTY)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Metoda sprawdza stan planszy
     * 
     * @return Stan planszy (WIN, RUN, PAT)
     */
    private GameState check() {
        GameState state = GameState.RUN; // domyslnie ustawiamy mozliwość wykonania ruchu

        // Czy sa jeszcze ruchy
        if(!hasEmpty()) {
            state = GameState.PAT;
        }

        // Sprawdzenie czy jest wygrany
        if(
            (
                // poziomo
                (
                    !gameBoard.get(findKey(new Coordinate(0, 0))).equals(EMPTY) && 
                    gameBoard.get(findKey(new Coordinate(0, 0))).equals(gameBoard.get(findKey(new Coordinate(0, 1)))) &&
                    gameBoard.get(findKey(new Coordinate(0, 0))).equals(gameBoard.get(findKey(new Coordinate(0, 2)))) 
                ) || (
                    !gameBoard.get(findKey(new Coordinate(1, 0))).equals(EMPTY) && 
                    gameBoard.get(findKey(new Coordinate(1, 0))).equals(gameBoard.get(findKey(new Coordinate(1, 1)))) &&
                    gameBoard.get(findKey(new Coordinate(1, 0))).equals(gameBoard.get(findKey(new Coordinate(1, 2))))
                ) || (
                    !gameBoard.get(findKey(new Coordinate(2, 0))).equals(EMPTY) && 
                    gameBoard.get(findKey(new Coordinate(2, 0))).equals(gameBoard.get(findKey(new Coordinate(2, 1)))) &&
                    gameBoard.get(findKey(new Coordinate(2, 0))).equals(gameBoard.get(findKey(new Coordinate(2, 2))))
                )
            ) || (
                // pionowo
                (
                    !gameBoard.get(findKey(new Coordinate(0, 0))).equals(EMPTY) && 
                    gameBoard.get(findKey(new Coordinate(0, 0))).equals(gameBoard.get(findKey(new Coordinate(1, 0)))) &&
                    gameBoard.get(findKey(new Coordinate(0, 0))).equals(gameBoard.get(findKey(new Coordinate(2, 0)))) 
                ) || (
                    !gameBoard.get(findKey(new Coordinate(0, 1))).equals(EMPTY) && 
                    gameBoard.get(findKey(new Coordinate(0, 1))).equals(gameBoard.get(findKey(new Coordinate(1, 1)))) &&
                    gameBoard.get(findKey(new Coordinate(0, 1))).equals(gameBoard.get(findKey(new Coordinate(2, 1))))
                ) || (
                    !gameBoard.get(findKey(new Coordinate(0, 2))).equals(EMPTY) && 
                    gameBoard.get(findKey(new Coordinate(0, 2))).equals(gameBoard.get(findKey(new Coordinate(1, 2)))) &&
                    gameBoard.get(findKey(new Coordinate(0, 2))).equals(gameBoard.get(findKey(new Coordinate(2, 2))))
                )
            ) || (
                // ukos
                (
                    !gameBoard.get(findKey(new Coordinate(0, 0))).equals(EMPTY) && 
                    gameBoard.get(findKey(new Coordinate(0, 0))).equals(gameBoard.get(findKey(new Coordinate(1, 1)))) &&
                    gameBoard.get(findKey(new Coordinate(0, 0))).equals(gameBoard.get(findKey(new Coordinate(2, 2))))
                ) || (
                    !gameBoard.get(findKey(new Coordinate(0, 2))).equals(EMPTY) && 
                    gameBoard.get(findKey(new Coordinate(0, 2))).equals(gameBoard.get(findKey(new Coordinate(1, 1)))) &&
                    gameBoard.get(findKey(new Coordinate(0, 2))).equals(gameBoard.get(findKey(new Coordinate(2, 0))))
                )
            )
        ) {
            state = GameState.WIN;
        }

        return state;
    }

    /**
     * Metoda ustawia wartośc w polu planszy
     * 
     * @param coord Koordynaty
     * @param sign Znak
     * 
     * @return Stan planszy
     */
    public GameState set(Coordinate coord, Sign sign) throws GameFieldNotEmptyException {
        Coordinate key = findKey(coord);
        if(!gameBoard.get(key).equals(EMPTY)) {
            throw new GameFieldNotEmptyException();
        }
        gameBoard.put(key, sign);

        return check();
    }

    /**
     * Klasa wewnętrzna przechowująca koordynaty znaku na planszy
     * 
     * @author pawel.apanasewicz@codeme.pl
     *
     */
    public static class Coordinate {

        /**
         * Wartość x
         */
        private int x;

        /**
         * Wartość y
         */
        private int y;

        /**
         * Konstruktor klasy
         * 
         * @param y Wartość y
         * @param x Wartość x
         */
        public Coordinate(int y, int x) {
            this.x = x;
            this.y = y;
        }

        /**
         * Metoda zwraca wartość x
         * 
         * @return Wartość x
         */
        public int getX() {
            return x;
        }

        /**
         * Metoda zwraca wartość y
         * 
         * @return Wartość y
         */
        public int getY() {
            return y;
        }

        /**
         * Nadpisana metoda porównująca obiekty Coordinate
         * 
         * @param object Obiekt do porównania
         * 
         * @return Czy obiekt jest podobny
         */
        public boolean equals(Object object) {
            if(object instanceof Coordinate) {
                Coordinate coord = (Coordinate)object;
                if(coord.getX() == x && coord.getY() == y) {
                    return true;
                }
            }

            return false;
        }

        /**
         * Metoda zwraca obiekt Coordinate zbudowany na podstawie podanego tekstu
         * 
         * @param text Koordynaty w postaci tekstu (np. 0;1)
         * 
         * @return Obiekt Coordenate
         */
        public static Coordinate getCoordenateByText(String text) {
            String[] textTab = text.split(",");

            return new Coordinate(Integer.valueOf(textTab[0]), Integer.valueOf(textTab[1]));
        }

    }

}
