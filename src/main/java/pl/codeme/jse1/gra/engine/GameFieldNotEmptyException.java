package pl.codeme.jse1.gra.engine;

/**
 * Wyjatek rzucany przy błedzie zwiazanym z ustawieniem wartości na planszy
 * 
 * @author paweł.apanasewicz@codeme.pl
 *
 */
public class GameFieldNotEmptyException extends Exception {

    private static final long serialVersionUID = -1702190104488850809L;

    /**
     * Konstruktor klasy
     */
    public GameFieldNotEmptyException() {
     // wywołanie konstruktora klasy Exception i ustawienie komunikatu wyjatku
        super("Podane koordynaty nie są puste podaj prawidłowe koordynaty!");
    }

}
