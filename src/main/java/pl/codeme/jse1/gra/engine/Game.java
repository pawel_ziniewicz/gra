package pl.codeme.jse1.gra.engine;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import pl.codeme.jse1.gra.engine.GameBoard.Coordinate;
import pl.codeme.jse1.gra.engine.GameBoard.GameState;
import pl.codeme.jse1.gra.ui.UInterface;

/**
 * Silnik gry
 * 
 * @author pawel.apanasewicz@codeme.pl
 *
 */
public class Game extends AbstractGame {

    /**
     * Pole przechowuje instancję gry (element wzorca singleton)
     */
    private static Game gameInstance;

    /**
     * Tablica graczy
     */
    private Player[] players;

    /**
     * Indeks bieżącego gracza
     */
    private int currentPlayerIndex;

    /**
     * Bieżący gracz
     */
    private Player currentPlayer;

    /**
     * Plansza gry
     */
    private GameBoard gameBoard;

    /**
     * Metoda zwracająca instancję klasy Game (element wzorca singleton)
     * 
     * @param uInterface Uchwyt do intrfejsu gracza
     * 
     * @return Instancję klasy Game
     */
    public static Game getInstance(UInterface uInterface) {
        if(gameInstance == null) { // jeśli nie ma jeszcze stworzonej instancji klasy Game to ją tworzymy
            gameInstance = new Game(uInterface);
        }

        return gameInstance;
    }

    /**
     * Pole przechowujące konfigurację gry
     */
    private GameConfigure config;

    /**
     * Interfejs łączoncy grę z graczem
     */
    private UInterface uInterface;

    /**
     * Konstruktor klasy
     * 
     * @param uInterface Uchwyt do interfejsu gracza
     */
    private Game(UInterface uInterface) {
        config = new GameConfigure();
        this.uInterface = uInterface;

        gameBoard = new GameBoard(
            Integer.valueOf(config.getProperty("height")), 
            Integer.valueOf(config.getProperty("width"))
        );

        // inicjalizacja wartości znaków z konfiguracji
        Sign.SIGN1.setSign(config.getProperty("sign1"));
        Sign.SIGN2.setSign(config.getProperty("sign2"));

        players = new Player[2];
        currentPlayerIndex = Integer.valueOf(config.getProperty("first")) - 1;

        setSaver(new DbJsonSaver());
    }

    /**
     * Metoda zwraca gracza wykonującego ruch
     * 
     * @return Gracz wykonujący ruch
     */
    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    /**
     * Metoda zwraca numer pierwszego nie ustawionego gracza z listy
     * 
     * @return Numer nieustawionego gracza
     */
    private int getNextEmptyPlayer() {
        for(int ix = 0; ix < players.length; ix++) {
            if(players[ix] == null) {
                return ix + 1;
            }
        }

        return 0;
    }

    /**
     * Metoda zwraca szerokość planszy
     * 
     * @return Szerokość planszy
     */
    public int getGameBoardWidth() {
        return gameBoard.getWidth();
    }

    /**
     * Metoda zwraca wysokość planszy
     * 
     * @return Wysokość planszy
     */
    public int getGameBoardHeight() {
        return gameBoard.getHeight();
    }

    /**
     * Metoda ustawia graczy
     * 
     * @param playerName Nazwa gracza
     */
    private void setPlayer(String name) {
        int playerNumber = getNextEmptyPlayer();
        Player player = new Player();
        player.setName(name);
        player.setSign(Sign.getSignByNumber(playerNumber));
        players[playerNumber - 1] = player;
    }

    /**
     * Metoda zmienia gracza na następnego w kolejce
     */
    private void changePlayer() {
        if(currentPlayerIndex < players.length - 1) {
            currentPlayerIndex++;
        } else {
            currentPlayerIndex = 0;
        }
        currentPlayer = players[currentPlayerIndex];
    }

    /**
     * Metoda wykonuje ruch gracza
     * 
     * @param message Komunikat podesłany od gracza
     * 
     * @return Komunikat zwrotny
     * 
     * @throws GameBoardSetException, GameWrongCoordinateException
     */
    private AvailableMessage turn(Message message) throws GameFieldNotEmptyException {
        AvailableMessage msg = AvailableMessage.TURN;
        GameState state = gameBoard.set(Coordinate.getCoordenateByText(message.getText()), currentPlayer.getSign());
        System.out.println("Stan gry: " + state);
        switch(state) {
        case PAT:
            msg = AvailableMessage.NEW;
            msg.setText("Remis, aby grać dalej wcisnij enter");
            break;
        case RUN:
            changePlayer();
            msg.setText("Ruch gracza " + currentPlayer.getName() + "( " + currentPlayer.getSign().getSign() + " )");
            msg.setGameBoardMap(gameBoard.getGameBoard());
            break;
        case WIN:
            msg = AvailableMessage.NEW;
            msg.setText("Wygrał gracz " + currentPlayer.getName() + "( " + currentPlayer.getSign().getSign() + " )!!!\n Aby kontynuować wciśnij enter.");
            break;
        }
        msg.setGameBoardMap(gameBoard.getGameBoard());

        return msg;
    }

    /**
     * Metoda do wysyłania wiadomości do gry
     * 
     * @param message Wiadomość do gry
     * 
     */
    @SuppressWarnings("unchecked")
    public void send(Message message) {
        AvailableMessage msg;
        if(message == null) {
            msg = AvailableMessage.PLAYER;
        } else {
            msg = (AvailableMessage)message;
        }

        // obsługa wyjątków jeżeli wystąpił
        if(msg.equals(AvailableMessage.ERROR)) {
            msg = (AvailableMessage)msg.getData();
            msg.setText(AvailableMessage.ERROR.getText());
        }

        System.out.println("UI: " + msg);
        try {
            switch(msg) {
            case END:
                System.exit(0);
                break;
            case LOAD:
                Map<String, Object> gameSave = (Map<String, Object>)load("game");
                Sign.SIGN1.setSign(gameSave.get("s1").toString());
                Sign.SIGN2.setSign(gameSave.get("s2").toString());
                currentPlayerIndex = Integer.valueOf(gameSave.get("pn").toString());
                currentPlayer = players[currentPlayerIndex];
                gameBoard.loadGameBoardMap((Map<Coordinate, Sign>) gameSave.get("gb"));
                msg = AvailableMessage.TURN;
                msg.setText("Ruch gracza " + currentPlayer.getName() + "( " + currentPlayer.getSign().getSign() + " )");
                msg.setGameBoardMap(gameBoard.getGameBoard());
                break;
            case NEW:
                gameBoard.clear();
                msg = AvailableMessage.TURN;
                msg.setText("Ruch gracza " + currentPlayer.getName() + "( " + currentPlayer.getSign().getSign() + " )");
                msg.setGameBoardMap(gameBoard.getGameBoard());
                break;
            case PLAYER:
                if(msg.getText() != null) {
                    setPlayer(msg.getText());
                }
                int playerNumber = getNextEmptyPlayer();
                if(playerNumber > 0) {
                    msg.setText("Podaj imię gracza " + playerNumber);
                } else {
                    msg = AvailableMessage.TURN;
                    currentPlayer = players[currentPlayerIndex];
                    msg.setText("Ruch gracza " + currentPlayer.getName() + "( " + currentPlayer.getSign().getSign() + " )");
                    msg.setGameBoardMap(gameBoard.getGameBoard());
                }
                break;
            case SAVE:
                // prygotowanie mapy z danymi gry
                Map<String, Object> data = new HashMap<>();
                data.put("pn", String.valueOf(currentPlayerIndex));
                data.put("s1", Sign.SIGN1.getSign());
                data.put("s2", Sign.SIGN2.getSign());
                data.put("gb", gameBoard.getGameBoard());
                // zapis gry
                if("CLI".equals(uInterface.getInterfaceName())) {
                    save("game", data);
                } else if("GUI".equals(uInterface.getInterfaceName())) {
                    save(msg.getText(), data);
                }
                msg = AvailableMessage.TURN;
                msg.setText("Gra została zapisana!\nRuch gracza " + currentPlayer.getName() + "( " + currentPlayer.getSign().getSign() + " )");
                msg.setGameBoardMap(gameBoard.getGameBoard());
                break;
            case TURN:
                msg = turn(msg);
                break;
            default:
                break;
            }
        } catch(GameFieldNotEmptyException | IOException e) {
            msg = AvailableMessage.ERROR;
            msg.setText(e.getMessage()); // pobranie komunikatu błedu
            msg.setData(AvailableMessage.TURN);
        }

        uInterface.send(msg); // wysłanie wiadomości do interfejsu gracza
    }

    public void setInterface(UInterface uInterface) {
        this.uInterface = uInterface;
    }

}
