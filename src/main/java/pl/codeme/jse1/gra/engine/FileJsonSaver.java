package pl.codeme.jse1.gra.engine;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;

import pl.codeme.jse1.gra.engine.GameBoard.Coordinate;

/**
 * Klasa zapisująca i wgrywająca z plików gier zapisanych w notacji JSON
 * 
 * @author pawel.apanasewicz@codeme.pl
 *
 */
public class FileJsonSaver implements Saver {

    /**
     * Uchwyt do katalogu zapisu
     */
    private String saveDir = ClassLoader.getSystemClassLoader().getResource("save").getPath();

    @SuppressWarnings("unchecked") // wyłaczenie ostrzeżeń o niepewnych rzutowaniach
    @Override
    public void save(String name, Object data) throws IOException {
        Map<String, Object> gameData = (Map<String, Object>)data;
        File file = new File(saveDir + "/" + name + ".json");
        if(!file.exists()) {
            file.createNewFile();
        }
        JsonObjectBuilder builder = Json.createObjectBuilder(); // utworzenie głównego obiektu JSON
        Iterator<String> keys = gameData.keySet().iterator(); // pobranie kluczy z mapy danych gry
        while(keys.hasNext()) {
            String fieldName = keys.next(); // pobranie klucza
            // jeżeli nazwa klucza jest gb to obsługujemy mapę planszy gry
            if(!fieldName.equals("gb")) {
                builder.add(fieldName, (String)gameData.get(fieldName)); // dodanie elementów do obiektu głownego JSON
            } else {
                JsonArrayBuilder array = Json.createArrayBuilder(); // przygotowanie tablicy na dane z planszy gry
                Map<Coordinate, Sign> gb = (Map<Coordinate, Sign>)gameData.get(fieldName); // pobranie planszy gry
                Iterator<Coordinate> gbKeys = gb.keySet().iterator();
                while(gbKeys.hasNext()) {
                    Coordinate coord = gbKeys.next();
                    JsonObjectBuilder gbBuilder = Json.createObjectBuilder(); // przygotowanie obiektu z koordynatami i znakiem
                    gbBuilder.add("x", coord.getX());
                    gbBuilder.add("y", coord.getY());
                    gbBuilder.add("v", gb.get(coord).toString());
                    array.add(gbBuilder); // dodanie do tablicy danych planszy
                }
                builder.add("gb", array); // dodanie tablicy danych z planszy do głównego obiektu JSON
            }
        }
        try(
            FileWriter fw = new FileWriter(file); // otwarcie pliku zapisu
            JsonWriter jwriter = Json.createWriter(fw); // spiecie pliku zapisu z parserem JSON
        ) {
            jwriter.writeObject(builder.build()); // zapis JSON-a do pliku
        } 
    }

    @Override
    public Object load(String name) throws IOException {
        Map<String, Object> data = new HashMap<>();
        File file = new File(saveDir + "/" + name + ".json");
        try(
            FileReader reader = new FileReader(file);
            JsonReader jreader = Json.createReader(reader);
        ) {
            JsonObject json = jreader.readObject();
            Iterator<String> keys = json.keySet().iterator();
            while(keys.hasNext()) {
                String field = keys.next();
                if(!field.equals("gb")) {
                    data.put(field, json.getString(field));
                } else {
                    Map<Coordinate, Sign> gb = new HashMap<>();
                    for(JsonValue item : json.getJsonArray(field)) {
                        JsonObject jobj = (JsonObject) item;
                        Coordinate key = new Coordinate(jobj.getInt("y"), jobj.getInt("x"));
                        gb.put(key, Sign.getSignByValue((jobj.getString("v").equals("EMPTY")) ? " " : jobj.getString("v")));
                    }
                    data.put("gb", gb);
                }
            }
        }

        return data;
    }

}
