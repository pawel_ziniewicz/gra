package pl.codeme.jse1.gra.engine;

import java.io.IOException;

/**
 * Interfejs obiektu do zapisu i wczytywania gier
 * 
 * @author pawel.apanasewicz@codeme.pl
 *
 */
public interface Saver {

    /**
     * Metoda zapisuje stan gry
     * 
     * @param name Nazwa gry
     * @param data Dane gry
     * @throws IOException
     */
    public void save(String name, Object data)  throws IOException ;

    /**
     * Metoda pobiera i zwraca zapisaną gre
     * 
     * @param name Nazwa gry
     * 
     * @return Dane zapisanej gry
     * @throws IOException
     */
    public Object load(String name) throws IOException ;

}
