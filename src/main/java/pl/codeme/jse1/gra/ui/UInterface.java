package pl.codeme.jse1.gra.ui;

import pl.codeme.jse1.gra.engine.Message;

/**
 * Interfejs gracza
 * 
 * @author pawel.apanasewicz@codeme.pl
 *
 */
public interface UInterface {

    /**
     * Metoda służy do wymiany informacji z interfejsem gracza
     * 
     * @param message Komunikat od gry
     */
    public void send(Message message);

    public String getInterfaceName();

}
