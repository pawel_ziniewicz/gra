package pl.codeme.jse1.gra.ui.scene;

import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 * Formatka umożliwiająca dodanie graczy i podanie im imion
 * 
 * @author pawel.apanasewicz@codeme.pl
 *
 */
public class PlayerScene extends AbstractScene {

    /**
     * Panel podstawowy sceny
     */
    private static GridPane root = new GridPane();

    /**
     * Etykieta na teksty odebrane od gry
     */
    private Label question;

    /**
     * Konstruktor klasy
     * 
     * @param stage Podstawa okna
     */
    public PlayerScene(Stage stage) {
        super(root, 300, 200);

        // Przygotowanie panelu podstawowego
        root.setHgap(4);
        root.setVgap(1);

        // Dodanie nagłówka
        root.add(new Label("Gra w kółko i krzyżyk"), 0, 0);
        // Przygotowanie etykiety na teksty debrane od gry
        question = new Label();
        // Przygotowanie pola na imie gracza
        TextField playerName = new TextField();
        // przycisk zapisujący gracza
        Button savePlayer = new Button("Zapisz");
        PlayerScene thisScene = this; // stworzenie zmiennej lokalne jako referencja do obiektu jako samego siebie

        // zbudowanie klasy anonimowej na bazie interfejsu i podstawienie go do obsługi zdarzeń zwiazanych z myszką
        savePlayer.setOnMouseClicked(new EventHandler<MouseEvent>() {

            @Override
            public void handle(MouseEvent event) {
                thisScene.message.setText(playerName.getText()); // ustawienie tekstu w komunikacie do gry
                playerName.setText(""); // czyszczenie pola na imię
                stage.hide(); // ukrycie podstawy okna
            }

        });

     // umieszczenie elementów na panelu głównym
        root.add(question, 0, 1);
        root.add(playerName, 0, 2);
        root.add(savePlayer, 0, 3);
    }

    @Override
    public void build() {
        question.setText(this.message.getText()); // ustawienie etykiety komunikatem odebranym od gry
    }

}
