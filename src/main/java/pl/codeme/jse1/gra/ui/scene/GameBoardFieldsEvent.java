package pl.codeme.jse1.gra.ui.scene;

import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import pl.codeme.jse1.gra.engine.GameBoard.Coordinate;
import pl.codeme.jse1.gra.engine.Sign;
import pl.codeme.jse1.gra.ui.GUInterface;

/**
 * Klasa obasługująca klikanie w pola planszy
 * 
 * @author pawel.apanasewicz@codeme.pl
 *
 */
public class GameBoardFieldsEvent implements EventHandler<MouseEvent> {

    /**
     * Przycisk turn
     */
    private Button turn;

    /**
     * Konstruktor klasy
     * 
     * @param turn Uchwyt do przycisku tury
     */
    public GameBoardFieldsEvent(Button turn) {
        this.turn = turn;
    }

    /**
     * Zablokuj pola planszy
     * 
     * @param parent Uchwyt do planszy
     * @param field Kliknięte pole planszy
     */
    private void lockFields(Parent parent, Node field) {
        lockFields(parent, field, false);
    }

    /**
     * Metoda umożliwiająca blokowanie i odblokowanie pól planszy
     * 
     * @param parent Uchwyt do planszy
     * @param field Klikniete pole
     * @param unlock Czy blokować czy odblokować
     */
    private void lockFields(Parent parent, Node field, boolean unlock) {
        GridPane board = (GridPane)parent;
        for(Node item : board.getChildren()) {
            if(unlock) {
                if(((TextField)item).getText().equals(Sign.EMPTY.getSign())) {
                    item.setDisable(false);
                }
            } else {
                if(!item.equals(field)) {
                    item.setDisable(true);
                }
            }
        }
    }

    @Override
    public void handle(MouseEvent event) {
        TextField field = (TextField)event.getSource(); // pobranie kliknietego przycisku

        // jeżeli kliknięte pole jest empty to wstawiamy znak w przeciwnym razie czyścimy
        if(field.getText().equals(Sign.EMPTY.getSign())) {
            field.setText(GUInterface.game.getCurrentPlayer().getSign().getSign()); // wstawienie znaku
            lockFields(field.getParent(), field); // blokada pól
            ((UserData)turn.getUserData()).setData(field); // przekazanie uchwytu do kliknietego pola do userData
            ((UserData)turn.getUserData()).setCoord((Coordinate)field.getUserData()); // Przekazanie koordynatów pola do userData
            turn.setDisable(false); // odblokowanie przycisku turn
        } else {
            field.setText(Sign.EMPTY.getSign());  // czyszczenie pola
            lockFields(field.getParent(), field, true); // odblokowanie pól
            turn.setDisable(true); // zablokowanie przycisku tury
        }
    }

}
