package pl.codeme.jse1.gra.ui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.Map;

import pl.codeme.jse1.gra.engine.Game;
import pl.codeme.jse1.gra.engine.GameBoard.Coordinate;
import pl.codeme.jse1.gra.engine.Message;
import pl.codeme.jse1.gra.engine.Sign;

/**
 * Interfejs CL gracza
 *  
 * @author pawel.apanasewicz@codeme.pl
 *
 */
public class CLInterface implements UInterface {

    /**
     * Pole przechowujące instancję Game
     */
    public static Game game;

    /**
     * Bufor do odczytu z konsoli
     */
    private BufferedReader reader;

    /**
     * Konstruktor klasy
     */
    public CLInterface() {
        game = Game.getInstance(this);
        InputStreamReader isr = new InputStreamReader(System.in);
        reader = new BufferedReader(isr);
    }

    /**
     * Metoda służąca do komunikacji z graczem
     * 
     * @param question Pytanie do gracza
     * 
     * @return Odpowiedz gracza
     */
    private String askUser(String question) {
        System.out.println(question);
        try {
            return reader.readLine(); // odczytanie z konsoli
        } catch(IOException e) { // blok uruchamiany w przypadku wystąpienia wyjątku IOException
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Metoda pobiera znak z planszy
     * 
     * @param gameBoard Plansza gry
     * @param coord Pozycja znaku
     * 
     * @return Znak z podanej pozycji
     */
    private String getSign(int y, int x, Map<Coordinate, Sign> gameBoard) {
        Coordinate coord = new Coordinate(y, x);
        Iterator<Coordinate> keys = gameBoard.keySet().iterator();
        while(keys.hasNext()) {
            Coordinate key = keys.next();
            if(key.equals(coord)) {
                System.out.println("# -" + gameBoard.get(key).getSign());
                return gameBoard.get(key).getSign();
            }
        }
        return gameBoard.get(coord).getSign();
    }

    /**
     * Metoda rysyjąca plansze
     * 
     * @param gameBoard Plansza gry
     */
    private void drowGameBoard(Map<Coordinate, Sign> gameBoard) {
        for(int x = 0; x < game.getGameBoardWidth(); x++) {
            if(x == 0) {
                System.out.print("  ");
            }
            System.out.print(x + " ");
        }
        System.out.println();
        for(int y = 0; y < game.getGameBoardHeight(); y++) {
            for(int x = 0; x < game.getGameBoardWidth(); x++) {
                if(x == 0) {
                    System.out.print(y + " ");
                }
                System.out.print(getSign(y, x, gameBoard));
                if(x < game.getGameBoardWidth() - 1) {
                    System.out.print("|");
                }
            }
            System.out.println();
            if(y < game.getGameBoardHeight() -1) {
                for(int x = 0; x < game.getGameBoardWidth(); x++) {
                    if(x == 0) {
                        System.out.print("  ");
                    }
                    System.out.print("-");
                    if(x < game.getGameBoardWidth() - 1) {
                        System.out.print("+");
                    }
                }
            }
            System.out.println();
        }
    }

    @Override
    public void send(Message message) { // Parametr message jest komunikatem wysłanym przez grę
        System.out.println("GAME: " + message);
        if(message.getGameBoardMap() != null) {
            drowGameBoard(message.getGameBoardMap());
        }

        String text = askUser(message.getText());
        Message msg = message.getMessageByText(text);
        if(msg == null) {
            msg = message;
        }

        msg.setText(text); // ustawienie wysyłanego tekstu
        game.send(msg); // wysłanie komunikatu do gry
    }

    public void start() {
        System.out.println("Gra w kółko i krzyżyk");
        game.send(null);
    }

    @Override
    public String getInterfaceName() {
        return "CLI";
    }

}
