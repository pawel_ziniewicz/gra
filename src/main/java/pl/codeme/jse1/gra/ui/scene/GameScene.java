package pl.codeme.jse1.gra.ui.scene;

import java.util.Iterator;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import pl.codeme.jse1.gra.engine.AvailableMessage;
import pl.codeme.jse1.gra.engine.GameBoard.Coordinate;
import pl.codeme.jse1.gra.engine.Sign;
import pl.codeme.jse1.gra.ui.GUInterface;

/**
 * Formatka gry
 * 
 * @author pawel.apanasewicz@codeme.pl
 *
 */
public class GameScene extends AbstractScene {

    /**
     * Panel podstawowy sceny
     */
    private static GridPane root = new GridPane();

    /**
     * Podstawa okna
     */
    private Stage stage;

    private boolean runBuild;

    private Label msg;

    private GridPane board;

    /**
     * Konstruktor klasy
     * 
     * @param stage Podstawa okna
     */
    public GameScene(Stage stage) {
        super(root, 400, 600); // wywołanie konstruktora z klasy po której dziedziczymy

        this.stage = stage;
        runBuild = false;

        // ustawienia rozmiaru panelu
        root.setVgap(4);
        root.setHgap(1);

    }

    private ObservableList<SaveGame> getSavedGames() {
        ObservableList<SaveGame> savedGames = FXCollections.observableArrayList();

        EntityManagerFactory factory = Persistence.createEntityManagerFactory("UsersDS");
        EntityManager em = factory.createEntityManager();

        TypedQuery<SaveGame> q = em.createNamedQuery(SaveGame.SAVED_GAMES, SaveGame.class);
        for(SaveGame item : q.getResultList()) {
            savedGames.add(item);
        }

        em.close();
        factory.close();

        return savedGames;
    }

    /**
     * Metoda tworzy menu z przyciskami
     * 
     * @return Menu
     */
    private Node getMenu() {
        HBox menu = new HBox();
        Button menuElement;

        EventHandler<MouseEvent> event = new MenuEvent(stage);
        // nowa gra
        menuElement = new Button("Nowa");
        menuElement.setUserData(new UserData(AvailableMessage.NEW));
        menuElement.setOnMouseClicked(event);
        menu.getChildren().add(menuElement);
        // koniec gry
        menuElement = new Button("Koniec");
        menuElement.setUserData(new UserData(AvailableMessage.END));
        menuElement.setOnMouseClicked(event);
        menu.getChildren().add(menuElement);
        // zapis gry
        TextField saveName = new TextField();
        saveName.setPrefWidth(70);
        menuElement = new Button("Save");
        menuElement.setUserData(saveName);
        menuElement.setPrefWidth(30);
        UserData ud = new UserData(AvailableMessage.SAVE);
        ud.setData(saveName);
        menuElement.setUserData(ud);
        menuElement.setOnMouseClicked(event);
        menu.getChildren().add(saveName);
        menu.getChildren().add(menuElement);
        // wczytanie gry
        ComboBox<SaveGame> savedGames = new ComboBox<>(getSavedGames());
        menu.getChildren().add(savedGames);

        return menu;
    }

    /**
     * Metoda tworzy planszę gry
     * 
     * @return Plansza gry
     */
    private Node getGameBoard() {
        VBox panel = new VBox();
        msg = new Label(this.message.getText());
        panel.getChildren().add(msg); // dodanie komunikatu od gry

        // przycisk tury
        Button turn = new Button("Tura");
        turn.setUserData(new UserData(AvailableMessage.TURN));
        turn.setOnMouseClicked(new MenuEvent(stage));
        turn.setDisable(true);

        // przygotowanie planszy
        board = new GridPane();
        board.setVgap(GUInterface.game.getGameBoardHeight());
        board.setHgap(GUInterface.game.getGameBoardWidth());

        // dodanie pól planszy
        Iterator<Coordinate> keys = this.message.getGameBoardMap().keySet().iterator();
        while(keys.hasNext()) {
            Coordinate coord = keys.next();
            TextField field = new TextField(this.message.getGameBoardMap().get(coord).getSign());
            field.setUserData(coord); // dodanie do pola klucza z gameBoardMap
            field.setEditable(false);
            field.setOnMouseClicked(new GameBoardFieldsEvent(turn));
            board.add(field, coord.getX(), coord.getY());
        }
        panel.getChildren().add(board);
        panel.getChildren().add(turn);

        return panel;
    }

    /**
     * Metoda odswieża wartości pol planszy
     */
    public void refreshFields() {
        for(Node node : board.getChildren()) {
            if(node instanceof TextField) {
                TextField field = (TextField)node;
                Coordinate coord = (Coordinate)field.getUserData();
                Sign sign = message.getGameBoardMap().get(coord);
                field.setText(sign.getSign());
                if(sign.equals(Sign.EMPTY)) {
                    field.setDisable(false);
                } else {
                    field.setDisable(true);
                }
            }
        }
    }

    @Override
    public void build() {
        // tylko raz dodaj poszczególne elementy okna
        if(!runBuild) {
            root.add(new Label("Gra w kółko i krzyżyk"), 0, 0); // tytuł
            root.add(getMenu(), 0, 1); // menu z przyciskami
            root.add(getGameBoard(), 0, 2); // plansza gry
            runBuild = true;
        }
        refreshFields();
        msg.setText(message.getText()); // ustawienie tekstu przesłanego od gry
    }

}
