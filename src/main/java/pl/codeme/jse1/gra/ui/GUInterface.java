package pl.codeme.jse1.gra.ui;

import javafx.application.Application;
import javafx.stage.Stage;
import pl.codeme.jse1.gra.engine.Game;
import pl.codeme.jse1.gra.engine.Message;
import pl.codeme.jse1.gra.ui.scene.AbstractScene;
import pl.codeme.jse1.gra.ui.scene.GameScene;
import pl.codeme.jse1.gra.ui.scene.PlayerScene;

/**
 * Intrgejs graficzny gracza
 * 
 * @author pawel.apanasewicz@codeme.pl
 *
 */
public class GUInterface extends Application implements UInterface {

    /**
     * Instancja silnika gry
     */
    public static Game game;

    /**
     * Podstawa okna
     */
    private Stage stage;

    /**
     * Scena prezentowana
     */
    private AbstractScene scene;

    /**
     * Czy wykonywany ruch
     */
    private boolean turn;

    @Override
    public void send(Message message) {
        if(message.getGameBoardMap() != null) {
            if(!turn) {
                scene = new GameScene(stage);
                turn = true;
            }
        }

        scene.setMessage(message); // ustawienie komunikatu gry
        stage.setScene(scene); // ustawienie sceny
        stage.showAndWait(); // zatrzymanie aplikacji i oczekiwanie na obsługa okna

        game.send(scene.getMessage()); // wysłanie komunikatu do silnika gry
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        GUInterface.game = Game.getInstance(this); // Stworzenie instancji gry i spięcie interfejsu z silnikiem gry

        this.stage = new Stage(); // stworzenie podstawy okna
        scene = new PlayerScene(stage); // na starcie inicjalizacja okna z graczami
        turn = false;

        game.send(null); // wysłanie rozpoczecie gry
    }

    public static void main(String[] args) {
        launch(args); // uruchomienie aplikacji
    }

    @Override
    public String getInterfaceName() {
        return "GUI";
    }
}
