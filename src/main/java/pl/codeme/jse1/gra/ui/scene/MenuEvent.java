package pl.codeme.jse1.gra.ui.scene;

import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import pl.codeme.jse1.gra.engine.Sign;

/**
 * Klasa obasługująca klikanie w przyciski menu i turn
 * 
 * @author pawel.apanasewicz@codeme.pl
 *
 */
public class MenuEvent implements EventHandler<MouseEvent> {

    /**
     * Podstawa okna
     */
    private Stage stage;

    /**
     * Konstruktor klasy
     * 
     * @param stage Podstawa okna
     */
    public MenuEvent(Stage stage) {
        this.stage = stage;
    }

    /**
     * Metoda odblokowuje pola planszy które są nie ustawione
     * 
     * @param parent Uchwyt do planszy
     */
    private void unlockFields(Parent parent) {
        GridPane board = (GridPane)parent;
        for(Node node : board.getChildren()) {
            // jeżeli pole jest puste to odblokuj w przeciwnym razie zablokuj
            if(((TextField)node).getText().equals(Sign.EMPTY.getSign())) {
                node.setDisable(false);
            } else {
                node.setDisable(true);
            }
        }
    }

    @Override
    public void handle(MouseEvent event) {
        Node node = (Node)event.getSource();
        UserData ud = (UserData)node.getUserData();
        // jesli to przycisk tura
        if(ud.isTurn()) {
            node.setDisable(true); // blokada przycisku tura
            unlockFields(((Node)ud.getData()).getParent()); // odblokowanie pól
        }
        AbstractScene scene = (AbstractScene)stage.getScene();
        scene.setMessage(ud.getMessage(), false); // pobranie komunikatu z userData bez uruchomienia build
        stage.hide(); // przeładowanie okna i wysłanie komunikatu do gry
    }

}
