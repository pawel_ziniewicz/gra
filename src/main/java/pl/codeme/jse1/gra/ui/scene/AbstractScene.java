package pl.codeme.jse1.gra.ui.scene;

import javafx.scene.Parent;
import javafx.scene.Scene;
import pl.codeme.jse1.gra.engine.Message;

/**
 * Klasa podstawa do budowy formatek do gry
 * 
 * @author pawel.apanasewicz@codeme.pl
 *
 */
public abstract class AbstractScene extends Scene {

    /**
     * Konstruktor klasy
     * 
     * @param root Panel podstawowy
     * @param width Szerokość okna
     * @param height Wysokość okna
     */
    public AbstractScene(Parent root, double width, double height) {
        super(root, width, height);
    }

    /**
     * Pole przechowujace komunikaty od gry
     */
    protected Message message;

    /**
     * Metoda uruchamiana w setMessage
     */
    protected abstract void build();

    /**
     * Metoda służąca do ustawienia komunikatu (przeciązona)
     * gry z uruchomieniem metody build
     * 
     * @param message Komunikat od gry
     */
    public void setMessage(Message message) {
        setMessage(message, true);
    }

    /**
     * Metoda służąca do ustawienia komunikatu
     * 
     * @param message Komunikat od gry
     * @param withBuild Czy uruchomić metod build
     */
    public void setMessage(Message message, boolean withBuild) {
        this.message = message;
        if(withBuild) {
            build();
        }
    }

    /**
     * Metoda zwraca ustawiony komunikat gry
     * 
     * @return Komunikat dla gry
     */
    public Message getMessage() {
        return message;
    }

}
