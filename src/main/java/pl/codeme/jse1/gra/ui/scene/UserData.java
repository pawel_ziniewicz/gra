package pl.codeme.jse1.gra.ui.scene;

import javafx.scene.control.TextField;
import pl.codeme.jse1.gra.engine.GameBoard.Coordinate;
import pl.codeme.jse1.gra.engine.Message;

/**
 * Obiekt do wymiany informacji w środowisku graficznym wykorzystując pole userData
 * 
 * @author pawel.apanasewicz@codeme.pl
 *
 */
public class UserData {

    /**
     * Komunikat gry
     */
    public Message message;

    /**
     * Tekst w komunikacie
     */
    public String text;

    /** 
     * Koordynaty pola
     */
    public Coordinate coord;

    /**
     * Pole do przechowywania różnych rzeczy
     */
    private Object data;

    /**
     * Konstruktor klasy
     * 
     * @param message komunikat gry
     */
    public UserData(Message message) {
        this.message = message;
    }

    /**
     * Metoda sprawdza czy obsługujemy ruch
     * 
     * @return Czy wykonujemy ruch
     */
    public boolean isTurn() {
        if(coord != null) {
            return true;
        }

        return false;
    }

    /**
     * Pobranie wypełnianego komunikatu do gry
     * 
     * @return Komunikat gry
     */
    public Message getMessage() {
        if(coord != null) {
            text = coord.getY() + "," + coord.getX();
        }
        if(coord == null && data != null && data instanceof TextField) {
            TextField field = (TextField)data;
            text = field.getText();
            field.setText("");
        }
        message.setText(text);

        return message;
    }

    /**
     * Metoda ustawia tekst komunikatu
     * 
     * @param text Tekst komunikatu
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * Metoda ustawia koordynaty pola
     * 
     * @param coord Koordynaty pola
     */
    public void setCoord(Coordinate coord) {
        this.coord = coord;
    }

    /**
     * Pobranie przechowywanego obiektu
     * 
     * @return Obiekt
     */
    public Object getData() {
        return data;
    }

    /**
     * Ustawianie obiektu
     * 
     * @param data Obiekt
     */
    public void setData(Object data) {
        this.data = data;
    }

}
