package pl.codeme.jse1.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class App {

    public static void main(String[] args) throws Exception {
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/codeme_jse21", "root", "");
        //Statement stm = conn.createStatement();
        //ResultSet res =  stm.executeQuery("SELECT * FROM user");
        PreparedStatement pstm = conn.prepareStatement(
            "SELECT u.* FROM user u INNER JOIN address a ON a.idu = u.id WHERE a.city LIKE ?"
        );
        pstm.setString(1, "Gd%");
        ResultSet res = pstm.executeQuery();
        int count = res.getMetaData().getColumnCount();
        for(int ix = 1; ix <= count; ix++) {
            System.out.print(res.getMetaData().getColumnName(ix) + "\t");
        }
        System.out.println("\n-------------------------------------");
        while(res.next()) {
            for(int ix = 1; ix <= count; ix++) {
                System.out.print(res.getString(ix) + "\t");
            }
            System.out.println();
        }
        pstm.close();
        //stm.close();
        conn.close();
    }

}
