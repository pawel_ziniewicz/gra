package pl.codeme.jse1.db.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import pl.codeme.jse1.db.jpa.entity.Address;
import pl.codeme.jse1.db.jpa.entity.User;

public class App {

    public static void main(String[] args) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("UsersDS");
        EntityManager em = factory.createEntityManager();

        User user = em.find(User.class, 2);
        System.out.println(user.getName() + " - " + user.getAge());
        user.setAge(16);
        em.getTransaction().begin();
        em.persist(user);
        em.getTransaction().commit();
        user = em.find(User.class, 2);
        System.out.println(user.getName() + " - " + user.getAge());
        user = new User();
        user.setName("Pies Pankracy");
        user.setAge(26);
        try {
            em.getTransaction().begin();
            em.persist(user);
            em.remove(user);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            e.printStackTrace();
        }
        TypedQuery<User> q = em.createNamedQuery(User.USER_T, User.class);
        q.setParameter("age", 0);
        List<User> res = q.getResultList();
        em.getTransaction().begin();
        for(User item : res) {
            System.out.println(item.getName() + " - " + item.getAge());
            item.setAge(21);
            em.persist(item);
            for(Address address : item.getAddresses()) {
                System.out.println(address.getStreet() + " " + address.getCity());
            }
        }
        em.getTransaction().commit();

        em.close();
        factory.close();
    }

}
