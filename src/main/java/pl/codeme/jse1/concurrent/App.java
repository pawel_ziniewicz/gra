package pl.codeme.jse1.concurrent;

public class App {

    public static void main(String[] args) {
        Speedway speedway = new Speedway();

        new Producer(speedway);
        new Starter(speedway);
    }

}
