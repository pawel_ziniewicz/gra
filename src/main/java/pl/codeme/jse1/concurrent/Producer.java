package pl.codeme.jse1.concurrent;

import java.util.Random;

import pl.codeme.jse1.oop.FordMustang;
import pl.codeme.jse1.oop.HondaCivic;

/**
 * Klasa produkująca auta (wątek)
 * 
 * @author pawel.apanasewicz@codeme.pl
 *
 */
public class Producer extends Thread {

    /**
     * Bloki startowe na torze
     */
    private Speedway speedway;

    /**
     * Konstruktor klasy
     * 
     * @param speedway Bloki startowe
     */
    public Producer(Speedway speedway) {
        this.speedway = speedway;
        start();
    }

    public void run() {
        try {
            Random r = new Random(10);
            // petla produkuje 10 aut
            for(int ix = 0; ix < 10; ix++) {
                Racer auto;
                // dla parzystego Ford w przeciwnym razie Honda
                if((r.nextInt() % 2) == 0) {
                    auto = new FordMustang();
                } else {
                    auto = new HondaCivic();
                }
                String autoName = auto.getClass().getName();
                auto.setName(autoName.substring(autoName.lastIndexOf(".") + 1) + " " + ix);
                speedway.put(auto); // wpuszczenie auta do bloku startowego
            }
            speedway.put(null); // wpuszczamy null informujemy, że konczymy produkcje aut
        } catch(InterruptedException e) {
            e.printStackTrace();
        }
    }

}
