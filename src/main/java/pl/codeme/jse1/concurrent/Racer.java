package pl.codeme.jse1.concurrent;

/**
 * Klasa umozliwiająca ściganie sie aut
 * 
 * @author pawel.apanasewicz@codeme.pl
 *
 */
public abstract class Racer implements Runnable {

    /**
     * Nazwa wyścigówki
     */
    private String name;

    /**
     * Dystans na którym ściga się auto
     */
    private int distance;

    /**
     * Dystans który pozostał do przejechania
     */
    private double remDistance;

    /**
     * Konstruktor klasy
     * 
     * @param distance Dystans do przejechania w metrach
     */
    public Racer(int distance) {
        this.distance = distance;
    }

    /**
     * Pobranie nazwy wyścigówki
     * 
     * @return Nazwa wyścigówki
     */
    public String getName() {
        return name;
    }

    /**
     * Metoda ustawia nazwę wyścigówki
     * 
     * @param name Nazwa wyścigówka
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Metoda symulująca jazdę auta
     * 
     * @return Dystans w m przejachany przez sekundę
     */
    public abstract double jedzie();

    @Override
    public void run() {
        remDistance = distance; // przygotowanie dystansu do przejechania
        while(true) {
            try {
                Thread.sleep(1000); // wstrzymanie na sekundę
                remDistance -= jedzie(); // odjęcie przejechanego dystansu przez sekundę
                System.out.println(name + ": " + "Pozostało " + remDistance + " m");
            } catch(InterruptedException e) {
                e.printStackTrace();
            }
            // jeśli dystans już jest przejechany przerywamy pętle
            if(remDistance <= 0) {
                System.out.println(name + ": " + "Meta!");
                break;
            }
        }
    }

}
