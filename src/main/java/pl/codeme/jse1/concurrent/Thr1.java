package pl.codeme.jse1.concurrent;

/**
 * Klasa wątku
 * @author pawel.apanasewicz@codeme.pl
 *
 */
public class Thr1 extends Thread {

    /**
     * Nadpisana metoda w klasie Thread uruchamiana jest przy starcie wątku
     */
    public void run() {
        try {
            Thread.sleep(1000); // zatrzymanie wątku na sekundę
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Thr1");
    }

}
