package pl.codeme.jse1.concurrent;

/**
 * Klasa wątku
 * @author pawel.apanasewicz@codeme.pl
 *
 */
public class Thr2 extends Thread { // Aby używać wątków dziedziczymy po klasie Thread

    /**
     * Nadpisana metoda w klasie Thread uruchamiana jest przy starcie wątku
     */
    public void run() {
        System.out.println("Thr2");
    }

}
