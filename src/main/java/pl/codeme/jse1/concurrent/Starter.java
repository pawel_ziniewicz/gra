package pl.codeme.jse1.concurrent;

/**
 * Klasa wypuszczająca auta (wątek)
 * 
 * @author pawel.apanasewicz@codeme.pl
 *
 */
public class Starter extends Thread {

    /**
     * Bloki startowe na torze
     */
    private Speedway speedway;

    /**
     * Konstruktor klasy
     * 
     * @param speedway Bloki startowe
     */
    public Starter(Speedway speedway) {
        this.speedway = speedway;
        start(); // uruchomienie wątka
    }

    public void run() {
        try {
            while(true) {
                // wystartowanie auta i sprawdzenie czy jeszcze jakieś jest jak nie ma to konieć pętli i wątka
                if(speedway.start(1) == null) {
                    break;
                }
            }
        } catch(InterruptedException e) {
            e.printStackTrace();
        }
    }
}
