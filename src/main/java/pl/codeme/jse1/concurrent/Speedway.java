package pl.codeme.jse1.concurrent;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Klasa bloków startowych toru
 * 
 * @author pawel.apanasewicz@codeme.pl
 *
 */
public class Speedway {

    /**
     * Kolejka bloków startowych
     */
    private Queue<Racer> startBoxes = new LinkedList<Racer>();

    /**
     * Czy dodana została wyścigówka
     */
    private boolean addRacer = false;

    /**
     * Czy producer przestał wpuszczać auta
     */
    private boolean done = false;

    /**
     * Metoda wrzuca do bloków startowych  auto
     * 
     * @param racer Racer
     * @throws InterruptedException
     */
    public synchronized void put(Racer racer) throws InterruptedException {
        // jeżeli racer równa się null to producer skończył wpuszczać auta
        if(racer == null) {
            done = true;
        } else {

            // jeżeli Starter jeszcze nie wystartował to producer czeka
            if(addRacer) {
                wait();
            }
    
            startBoxes.add(racer); // dodaje do bloku startowego auto
            System.out.println("Na starcie " + racer.getName());
        }

        addRacer = true; // info że dodałem
        notify(); // odblokowanie Startera
    }

    /**
     * Metoda wypuszcza auta z kolejki
     * 
     * @return Wypuszczone auto
     * @throws InterruptedException
     */
    public synchronized Racer start(int p) throws InterruptedException {
        // jeżeli producer przestał dodawać już wychodzimy
        if(done) {
            return null;
        }

        // jeśli Producer jeszcze dodaje to czekam
        if(!addRacer) {
            wait();
        }

        Racer racer = startBoxes.poll(); // pobranie auta
        if(racer != null) {
            Thread thr = new Thread(racer);
            thr.setPriority(p);
            thr.start();
            System.out.println("Wystartował " + racer.getName());
        }

        addRacer = false; // ustawiam info dla Producera sam się blokuję
        notify(); // sciagam wait z Producera

        return racer;
    }

}
