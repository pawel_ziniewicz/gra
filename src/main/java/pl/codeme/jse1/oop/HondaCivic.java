package pl.codeme.jse1.oop;

/**
 * Klasa reprezentująca Honde Civic w wersji sportowej (implementacja interfejsu WersjaTypeR)
 * 
 * @author pawel.apanasewicz@codeme.pl
 *
 */
public class HondaCivic extends Honda {

    /**
     * Konstruktor klasy
     */
    public HondaCivic() {
        super(1700, 800);
        runTurbin(90);
    }

}
