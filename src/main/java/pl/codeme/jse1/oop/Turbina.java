package pl.codeme.jse1.oop;

/**
 * Intrfejs sportowej linii aut
 * 
 * @author pawel.apanasewicz@codeme.pl
 *
 */
public interface Turbina {

    /**
     * Metoda zwiększająca prędkość auta
     * 
     * @param percent Procentowe zwiększenie prędkości
     */
    public void runTurbin(int percent);

}
