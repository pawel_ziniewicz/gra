package pl.codeme.jse1.gra.engine;

import java.util.Iterator;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import pl.codeme.jse1.gra.engine.GameBoard.Coordinate;
import pl.codeme.jse1.gra.ui.UInterface;

/**
 * Klasa testująca silnik gry
 * 
 * @author pawel.apanasewicz@codeme.pl
 *
 */
public class GameTest implements UInterface {

    /**
     * Instancja gry
     */
    private static Game game;

    /**
     * Komunikat przychodzący od gry
     */
    private Message gameMessage;

    /**
     * Metoda inicjalizująca testy
     * 
     * @throws GameConfigFileNotFoundException
     */
    @Before
    public void gameInit() {
        game = Game.getInstance(this);
        game.setSaver(new FileJsonSaver());
    }

    /**
     * Metoda pomocnicza pobierająca klucze z mapy gry
     * 
     * @param y Pozycja Y
     * @param x Pozycja X
     * 
     * @return Kordynaty klucz
     */
    private Coordinate getGameBoradKey(int y, int x) {
        Iterator<Coordinate> keys = gameMessage.getGameBoardMap().keySet().iterator();
        while(keys.hasNext()) {
            Coordinate coord = keys.next();
            Coordinate example = new Coordinate(y, x);
            if(coord.equals(example)) {
                return coord;
            }
        }

        return null;
    }

    /**
     * Metoda testująca wymianę komunikatów w grze
     */
    @Test
    public void messagesTest() {
        game.send(null); // rozpoczęcie gry
        Assert.assertEquals(AvailableMessage.PLAYER, gameMessage); // sprawdzenie poprawności komunikatu
        // nazwanie gracza pierwszego
        Message msg = AvailableMessage.PLAYER;
        msg.setText("Antek");
        game.send(msg);
        Assert.assertEquals("Podaj imię gracza 2", gameMessage.getText()); // sprawdzenie przysłanego tekstu
        Assert.assertEquals(AvailableMessage.PLAYER, gameMessage); // sprawdzenie poprawności komunikatu
        // Nazwanie gracza drugiego
        msg = AvailableMessage.PLAYER;
        msg.setText("Ola");
        game.send(msg);
        Assert.assertEquals(AvailableMessage.TURN, gameMessage); // sprawdzenie poprawności komunikatu
        Assert.assertEquals("Antek", game.getCurrentPlayer().getName()); // sprawdzenie nazwy gracza rozpoczynającego
        Assert.assertEquals('O', game.getCurrentPlayer().getSign().getSign()); // sprawdzenie znaku gracza rozpoczynającego
        // rozegranie pierwszej tury
        msg = AvailableMessage.TURN;
        msg.setText("1,1");
        game.send(msg);
        Coordinate key = getGameBoradKey(1, 1);
        Assert.assertEquals(Sign.SIGN1, gameMessage.getGameBoardMap().get(key)); // sprawdzenie zaznaczenia
    }

    @Override
    public void send(Message message) {
        this.gameMessage = message;
    }

    @Override
    public String getInterfaceName() {
        return "CLI";
    }

}
